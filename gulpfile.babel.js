// generated on 2016-03-03 using generator-webapp 2.0.0
import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import browserSync from 'browser-sync';
import del from 'del';
import {stream as wiredep} from 'wiredep';
import jade from 'gulp-jade';
import data from 'gulp-data';
import fs from 'fs';
import gutil from 'gulp-util';
import requireDir from 'require-dir';
import ipt from 'yargs';

const $ = gulpLoadPlugins();
const reload = browserSync.reload;
const dir = requireDir('./tasks');

gulp.task('test', ['update'], () => {
	var text, type, siteMapPath, jadeClonePath, pagePath, modulePath, includePath;
	siteMapPath = 'app/jade/data/sitemap.json';
	jadeClonePath = 'app/jade/pages/clone.jade';
	pagePath = {
		jade: 'app/jade/pages/',
		scss: 'app/styles/pages/',
		js: 'app/scripts/pages/',
	};
	modulePath = {
		jade: 'app/jade/modules/',
		scss: 'app/styles/modules/',
	};
	includePath = {
		jade: 'app/jade/global/modules.jade',
		scss: 'app/styles/main.scss'
	};
});

gulp.task('jade', () => {
	return gulp.src('app/jade/pages/*.jade')
		.pipe($.plumber())
		.pipe(data(function(file) {
			return JSON.parse(fs.readFileSync('./app/jade/data/sitemap.json', 'utf-8'));
		}))
		.pipe(data(function(file) {
			return JSON.parse(fs.readFileSync('./app/jade/data/data.json', 'utf-8'));
		}))
		.pipe(jade({
			pretty: '\t'
		}))
		.pipe(gulp.dest('.tmp'))
		.pipe(reload({stream: true}));
});

gulp.task('jade-build', () => {
	return gulp.src(['app/jade/pages/*.jade', '!app/jade/pages/clone.jade'])
		.pipe(data(function(file) {
			var data = JSON.parse(fs.readFileSync('./app/jade/data/data.json', 'utf-8'));
			data.sitemap = false;
			return data;
		}))
		.pipe(jade({
			pretty: '\t'
		}))
		.pipe(gulp.dest('.tmp'));
});

gulp.task('styles', () => {
	return gulp.src('app/styles/*.scss')
		.pipe($.plumber())
		.pipe($.sourcemaps.init())
		.pipe($.sass.sync({
			outputStyle: 'expanded',
			precision: 10,
			includePaths: ['.']
		}).on('error', $.sass.logError))
		.pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
		.pipe($.sourcemaps.write())
		.pipe(gulp.dest('.tmp/styles'))
		.pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
	return gulp.src('app/scripts/**/*.js')
		.pipe($.plumber())
		.pipe($.sourcemaps.init())
		.pipe($.babel())
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest('.tmp/scripts'))
		.pipe(reload({stream: true}));
});

function lint(files, options) {
	return () => {
		return gulp.src(files)
			.pipe(reload({stream: true, once: true}))
			.pipe($.eslint(options))
			.pipe($.eslint.format())
			.pipe($.if(!browserSync.active, $.eslint.failAfterError()));
	};
}
const testLintOptions = {
	env: {
		mocha: true
	}
};

gulp.task('lint', lint('app/scripts/**/*.js'));
gulp.task('lint:test', lint('test/spec/**/*.js', testLintOptions));

gulp.task('html', ['jade', 'styles', 'scripts'], () => {
	return gulp.src('.tmp/*.html')
		.pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
		.pipe($.if('*.js', $.uglify()))
		.pipe($.if('*.css', $.cssnano()))
		.pipe($.if('*.html', $.htmlmin({collapseWhitespace: true})))
		.pipe(gulp.dest('dist'));
});

gulp.task('html-dev', ['jade-build', 'styles', 'scripts'], () => {
	return gulp.src('.tmp/*.html')
		.pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
		.pipe($.if('*.css', $.cssnano()))
		.pipe(gulp.dest('dist'));
});

gulp.task('images', () => {
	return gulp.src('app/images/**/*')
		.pipe($.cache($.imagemin({
			progressive: true,
			interlaced: true,
			// don't remove IDs from SVGs, they are often used
			// as hooks for embedding and styling
			svgoPlugins: [{cleanupIDs: false}]
		})))
		.pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', () => {
	return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
		.concat('app/fonts/**/*'))
		.pipe(gulp.dest('.tmp/fonts'))
		.pipe(gulp.dest('dist/fonts'));
});

gulp.task('extras', () => {
	return gulp.src([
		'app/*.*'
		/*add dynamic resource here to copy to dist*/
	], {
		dot: true
	}).pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('serve', ['jade', 'styles', 'scripts', 'fonts'], () => {
	browserSync({
		notify: true,
		port: 9000,
		server: {
			baseDir: ['.tmp', 'app'],
			routes: {
				'/bower_components': 'bower_components'
			}
		}
	});

	gulp.watch(['app/images/**/*']).on('change', reload);

	gulp.watch('app/jade/**/*.jade', ['jade']);
	gulp.watch('app/jade/data/*.json', ['jade']);
	gulp.watch('app/styles/**/*.scss', ['styles']);
	gulp.watch('app/scripts/**/*.js', ['scripts']);
	gulp.watch('app/fonts/**/*', ['fonts']);
	gulp.watch('bower.json', ['wiredep', 'fonts']);
});

// inject bower components
gulp.task('wiredep', () => {
	gulp.src('app/styles/*.scss')
		.pipe(wiredep({
			ignorePath: /^(\.\.\/)+/
		}))
		.pipe(gulp.dest('app/styles'));

	gulp.src('app/global/*.jade')
		.pipe(wiredep({
			ignorePath: /^(\.\.\/)*\.\./
		}))
		.pipe(gulp.dest('app/global'));
});

gulp.task('build', ['lint', 'html', 'images', 'fonts', 'extras'], () => {
	return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('build-dev', ['lint', 'html-dev', 'images', 'fonts', 'extras'], () => {
	return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clean'], () => {
	var task = ipt.argv.dev ? 'build-dev' : 'build';
	gulp.start(task);
});