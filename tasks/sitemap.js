import gulp from 'gulp';
import fs from 'fs';
import gutil from 'gulp-util';
import ipt from 'yargs';

gulp.task('sitemap', () => {
	if (ipt.argv.renew) {
		var siteMapPath, pagePath, modulePath;
		siteMapPath = 'app/jade/data/sitemap.json';
		pagePath = {
			jade: 'app/jade/pages/',
			scss: 'app/styles/pages/',
			js: 'app/scripts/pages/',
		};
		modulePath = {
			jade: 'app/jade/modules/',
			scss: 'app/styles/modules/',
		};

		// define files and sitemap
		var data = { pages: [], modules: [] };

		var pages = data.pages;
		var modules = data.modules;
		var pageDir = fs.readdirSync(pagePath.jade);
		var moduleDir = fs.readdirSync(modulePath.jade);

		// first log
		gutil.log('Renew %s ...', gutil.colors.cyan('site-map'));

		// update pages sitemap
		updateSitemap(pageDir, pages, 'page');

		// update modules sitemap
		updateSitemap(moduleDir, modules, 'module');

		// update the data
		var resData = JSON.stringify(data, null, '\t');
		fs.writeFileSync(siteMapPath, resData);
		gutil.log('%s up to date!', gutil.colors.cyan('site-map'));

		function updateSitemap(files, list, type) {
			// add to sitemap if file exists but not in list
			for (var i = files.length - 1; i >= 0; i--) {
				var file = files[i];
				var found = false;
				file = file.replace('.jade', '');

				for (var j = list.length - 1; j >= 0; j--) {
					var item = list[j];
					if (file == item.name) {
						found = true;
						break;
					}
				}
				if (!found) {
					var item = {name: file};
					if (type == 'page') {
						item.url = file + '.html';
					}

					list.push(item);
					console.log('- Updated %s, %s %s %s', gutil.colors.cyan('site-map'), gutil.colors.green('add'), gutil.colors.cyan(type), gutil.colors.yellow(item.name));
				}
			}
		}
	}
	else {
		var siteMapPath, pagePath, modulePath;
		siteMapPath = 'app/jade/data/sitemap.json';
		pagePath = {
			jade: 'app/jade/pages/',
			scss: 'app/styles/pages/',
			js: 'app/scripts/pages/',
		};
		modulePath = {
			jade: 'app/jade/modules/',
			scss: 'app/styles/modules/',
		};

		// define files and sitemap
		var data;
		try {
			data = JSON.parse(fs.readFileSync(siteMapPath, 'utf-8'));
		}
		catch(e) { }

		data = data ? data : { pages: [], modules: [] };

		var pages = data.pages;
		var modules = data.modules;
		var pageDir = fs.readdirSync(pagePath.jade);
		var moduleDir = fs.readdirSync(modulePath.jade);
		var firstLog = true;

		// update pages sitemap
		updateSitemap(pageDir, pages, 'page');

		// update modules sitemap
		updateSitemap(moduleDir, modules, 'module');

		// update the data
		var resData = JSON.stringify(data, null, '\t');
		fs.writeFileSync(siteMapPath, resData);
		if (!firstLog) {
			console.log('>> %s updated to date!\n', gutil.colors.cyan('site-map'));
		}
		else {
			gutil.log('%s up to date!', gutil.colors.cyan('site-map'));
		}

		function updateSitemap(files, list, type) {
			// add to sitemap if file exists but not in list
			for (var i = files.length - 1; i >= 0; i--) {
				var file = files[i];
				var found = false;
				file = file.replace('.jade', '');

				for (var j = list.length - 1; j >= 0; j--) {
					var item = list[j];
					if (file == item.name) {
						found = true;
						break;
					}
				}
				if (!found) {
					var item = {name: file};
					if (type == 'page') {
						item.url = file + '.html';
					}

					list.push(item);
					if (firstLog) {
						firstLog = false;
						gutil.log('%s %s %s', gutil.colors.cyan('site-map'), gutil.colors.red('is mismake with'), gutil.colors.magenta('files'));
					}
					console.log('- Updated %s, %s %s %s', gutil.colors.cyan('site-map'), gutil.colors.green('add'), gutil.colors.cyan(type), gutil.colors.yellow(item.name));
				}
			}

			// remove from sitemap if file not exists
			for (var i = list.length - 1; i >= 0; i--) {
				var item = list[i];
				var found = false;

				for (var j = files.length - 1; j >= 0; j--) {
					var file = files[j];
					file = file.replace('.jade', '');
					if (file == item.name) {
						found = true;
						break;
					}
				}
				if (!found) {
					list.splice(i, 1);
					if (firstLog) {
						firstLog = false;
						gutil.log('%s %s %s', gutil.colors.cyan('site-map'), gutil.colors.red('is mismake with'), gutil.colors.magenta('files'));
					}
					console.log('- Updated %s, %s %s %s', gutil.colors.cyan('site-map'), gutil.colors.red('remove'), gutil.colors.cyan(type), gutil.colors.yellow(item.name));
				}
			}
		}
	}

	return gulp;
});