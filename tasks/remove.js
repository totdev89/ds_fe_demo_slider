import gulp from 'gulp';
import ipt from 'yargs';
import fs from 'fs';
import gutil from 'gulp-util';
import del from 'del';

gulp.task('remove', () => {
	var type, siteMapPath, pagePath, modulePath, includePath, filename;
	siteMapPath = 'app/jade/data/sitemap.json';
	pagePath = {
		jade: 'app/jade/pages/',
		scss: 'app/styles/pages/',
		js: 'app/scripts/pages/',
	};
	modulePath = {
		jade: 'app/jade/modules/',
		scss: 'app/styles/modules/',
	};
	includePath = {
		jade: 'app/jade/global/modules.jade',
		scss: 'app/styles/main.scss'
	};
	if (ipt.argv.module) {
		type = 'module';
		filename = ipt.argv.module;

		// remove file
		var jadePath = modulePath.jade + filename + '.jade';
		var scssPath = modulePath.scss + '_' + filename + '.scss';
		del([jadePath, scssPath]);
		gutil.log('Removed %s named \'%s\'', gutil.colors.cyan(type), gutil.colors.green(filename));
		console.log('- Delete %s file at \'%s\'', gutil.colors.cyan('jade'), gutil.colors.yellow(jadePath));
		console.log('- Delete %s file at \'%s\'', gutil.colors.cyan('scss'), gutil.colors.yellow(scssPath));

		// update include
		var jadeInclude = fs.readFileSync(includePath.jade, 'utf-8');
		var cmtPattern = '\n//- Added by \'gulp add module\': ' + filename;
		var txtPattern = '\ninclude ../modules/' + filename;
		jadeInclude = jadeInclude.replace(cmtPattern, '');
		jadeInclude =  jadeInclude.replace(txtPattern, '');
		fs.writeFileSync(includePath.jade, jadeInclude);
		console.log('- Updated include %s for %s at \'%s\'', gutil.colors.cyan('jade'), gutil.colors.cyan(filename), gutil.colors.yellow(jadePath));

		var scssInclude = fs.readFileSync(includePath.scss, 'utf-8');
		var cmtPattern = '\n// Added by \'gulp add module\': ' + filename;
		var txtPattern = '\n@import "modules/' + filename + '";';
		scssInclude = scssInclude.replace(cmtPattern, '');
		scssInclude = scssInclude.replace(txtPattern, '');
		fs.writeFileSync(includePath.scss, scssInclude);
		console.log('- Updated include %s for %s at \'%s\'', gutil.colors.cyan('scss'), gutil.colors.cyan(filename), gutil.colors.yellow(scssPath));

		// update site-map
		console.log('- To update %s, use: $ gulp sitemap %s\n', gutil.colors.cyan('site-map'), gutil.colors.cyan('--renew'));
	}
	else if (ipt.argv.page) {
		type = 'page';
		filename = ipt.argv.page;

		// remove file
		var jadePath = pagePath.jade + filename + '.jade';
		var scssPath = pagePath.scss + '_' + filename + '.scss';
		var jsPath = pagePath.js + filename + '.js';
		del([jadePath, scssPath, jsPath]);
		gutil.log('Removed %s named \'%s\'', gutil.colors.cyan(type), gutil.colors.green(filename));
		console.log('- Delete %s file at \'%s\'', gutil.colors.cyan('jade'), gutil.colors.yellow(jadePath));
		console.log('- Delete %s file at \'%s\'', gutil.colors.cyan('scss'), gutil.colors.yellow(scssPath));
		console.log('- Delete %s file at \'%s\'', gutil.colors.cyan('js'), gutil.colors.yellow(jsPath));

		// update include
		var scssInclude = fs.readFileSync(includePath.scss, 'utf-8');
		var cmtPattern = '\n// Added by \'gulp add page\': ' + filename;
		var txtPattern = '\n@import "pages/' + filename + '";';
		scssInclude = scssInclude.replace(cmtPattern, '');
		scssInclude = scssInclude.replace(txtPattern, '');
		fs.writeFileSync(includePath.scss, scssInclude);
		console.log('- Updated include %s for %s at \'%s\'', gutil.colors.cyan('scss'), gutil.colors.cyan(filename), gutil.colors.yellow(scssPath));

		// update site-map
		console.log('- To update %s, use: $ gulp sitemap %s\n', gutil.colors.cyan('site-map'), gutil.colors.cyan('--renew'));
	}
})