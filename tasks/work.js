import gulp from 'gulp';
import ipt from 'yargs';
import fs from 'fs';
import gutil from 'gulp-util';

gulp.task('work', () => {
	var workPath, fileName, data;
	workPath = 'app/jade/data/work.json';

	if (ipt.argv.file) {
		var temp = ipt.argv.file;

		if (/,/g.test(temp)) {
			// case multi files
		}
		else {
			// case single file
		}
	}
	else {
		gutil.log('To choose %s file to work, please use those methods below:', gutil.colors.cyan('jade'));
		console.log('- Choose multiple files: $ gulp work %s %s,%s,..', gutil.colors.cyan('--file'), gutil.colors.yellow('file-name-1'),  gutil.colors.yellow('file-name-2'));
		console.log('- Choose single file: $ gulp work %s %s', gutil.colors.cyan('--file'), gutil.colors.yellow('file-name'));
		console.log('- Choose all files: $ gulp work $s\n', gutil.colors.cyan('--all'));
		return;
	}
});