import gulp from 'gulp';
import ipt from 'yargs';
import fs from 'fs';
import gutil from 'gulp-util';

gulp.task('add', ['sitemap'], () => {
	var text, type, siteMapPath, jadeClonePath, pagePath, modulePath, includePath;
	siteMapPath = 'app/jade/data/sitemap.json';
	jadeClonePath = 'app/jade/pages/clone.jade';
	pagePath = {
		jade: 'app/jade/pages/',
		scss: 'app/styles/pages/',
		js: 'app/scripts/pages/',
	};
	modulePath = {
		jade: 'app/jade/modules/',
		scss: 'app/styles/modules/',
	};
	includePath = {
		jade: 'app/jade/global/modules.jade',
		scss: 'app/styles/main.scss'
	};

	// check params
	if (ipt.argv.module) {
		text = ipt.argv.module;
		type = 'module';
	} else if (ipt.argv.page) {
		text = ipt.argv.page;
		type = 'page';
	} else {
		gutil.log('To add new %s, please use those methods below:', gutil.colors.cyan('object'));
		console.log('- Add new module: $ gulp add %s %s', gutil.colors.cyan('--module'), gutil.colors.yellow('new-module-name-here'));
		console.log('- Add new page: $ gulp add %s %s\n', gutil.colors.cyan('--page'), gutil.colors.yellow('new-page-name-here'));
		return;
	}

	// handle add object
	if (text) {
		var data = JSON.parse(fs.readFileSync(siteMapPath, 'utf-8'));
		// case page type
		if (type == 'page') {
			var page = {name: text, url: text + '.html'};
			// check if the name has exists
			for (var i = 0; i < data.pages.length; i++) {
				var currentPage = data.pages[i];
				if (currentPage.name == page.name) {
					console.log('%s \'%s\' %s', gutil.colors.red('Page'), gutil.colors.green(text), gutil.colors.red('is already exists! Please change another name.\n'));
					return;
				}
			}

			// its ok to create file
			// - update sitemap
			data.pages.push(page);
			fs.writeFileSync(siteMapPath, JSON.stringify(data));
			// - add physic file
			var jadePath = pagePath.jade + page.name + '.jade';
			var scssPath = pagePath.scss + '_' + page.name + '.scss';
			var jsPath = pagePath.js + page.name + '.js';
			//
			var jadeContent = fs.readFileSync(jadeClonePath, 'utf-8');
			jadeContent = jadeContent.replace(/clone/g, page.name);
			var scssContent = '.page-' + page.name + ' {';
			scssContent += '\n}';
			var jsContent = 'use strict;\n';
			jsContent += 'console.log("page ' + page.name + ' hello!")';
			fs.writeFileSync(jadePath, jadeContent);
			fs.writeFileSync(scssPath, scssContent);
			fs.writeFileSync(jsPath, jsContent);
			// - update config/include
			var scssInclude = fs.readFileSync(includePath.scss, 'utf-8');
			scssInclude += '\n// Added by \'gulp add page\': ' + page.name;
			scssInclude += '\n@import "pages/' + page.name + '";';
			fs.writeFileSync(includePath.scss, scssInclude);

			// - logs
			gutil.log('Added %s named \'%s\', url: \'%s\' ', gutil.colors.cyan(type), gutil.colors.green(text), gutil.colors.magenta(page.url));
			console.log('- Created %s file at %s', gutil.colors.cyan('jade'), gutil.colors.yellow(jadePath));
			console.log('- Created %s file at %s', gutil.colors.cyan('scss'), gutil.colors.yellow(scssPath));
			console.log('- Created %s file at %s', gutil.colors.cyan('js'), gutil.colors.yellow(jsPath));
			console.log('- Updated include %s for %s at %s', gutil.colors.cyan('scss'), gutil.colors.cyan(text), gutil.colors.yellow(includePath.scss));
			console.log('- Updated %s %s\n', gutil.colors.cyan('sitemap-path'), gutil.colors.yellow(siteMapPath));
		}
		// case module type
		else {
			var module = {name: text};
			// check if the name has exists
			for (var i = 0; i < data.modules.length; i++) {
				var currentModule = data.modules[i];
				if (currentModule.name == module.name) {
					console.log('%s \'%s\' %s', gutil.colors.red('Module'), gutil.colors.green(text), gutil.colors.red('is already exists! Please change another name.\n'));
					return;
				}
			}
			// its ok to create file
			// - update sitemap
			data.modules.push(module);
			fs.writeFileSync(siteMapPath, JSON.stringify(data));
			// - add physic file
			var jadePath = modulePath.jade + module.name + '.jade';
			var scssPath = modulePath.scss + '_' + module.name + '.scss';
			//
			var jadeContent = '//- module ' + module.name;
			jadeContent += '\n//- use: +block-' + module.name;
			jadeContent += '\nmixin block-' + module.name;
			jadeContent += '\n\t.block-' + module.name;
			var scssContent = '.block-' + module.name + ' {';
			scssContent += '\n}';
			fs.writeFileSync(jadePath, jadeContent);
			fs.writeFileSync(scssPath, scssContent);
			// - update config/include
			var scssInclude = fs.readFileSync(includePath.scss, 'utf-8');
			scssInclude += '\n// Added by \'gulp add module\': ' + module.name;
			scssInclude += '\n@import "modules/' + module.name + '";';
			fs.writeFileSync(includePath.scss, scssInclude);

			var jadeInclude = fs.readFileSync(includePath.jade, 'utf-8');
			jadeInclude += '\n//- Added by \'gulp add module\': ' + module.name;
			jadeInclude += '\ninclude ../modules/' + module.name;
			fs.writeFileSync(includePath.jade, jadeInclude);

			// - logs
			gutil.log('Added %s named \'%s\'', gutil.colors.cyan(type), gutil.colors.green(text));
			console.log('- Created %s file at %s', gutil.colors.cyan('jade'), gutil.colors.yellow(jadePath));
			console.log('- Created %s file at %s', gutil.colors.cyan('scss'), gutil.colors.yellow(scssPath));
			console.log('- Updated include %s for %s at %s', gutil.colors.cyan('jade'), gutil.colors.cyan(text), gutil.colors.yellow(includePath.jade));
			console.log('- Updated include %s for %s at %s', gutil.colors.cyan('scss'), gutil.colors.cyan(text), gutil.colors.yellow(includePath.scss));
			console.log('- Updated %s %s\n', gutil.colors.cyan('sitemap-path'), gutil.colors.yellow(siteMapPath));
		}
		return;
	}
});